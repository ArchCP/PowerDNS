SELECT
	"Dns"."Record"."OrderName",
	lower("Dns"."Record"."Name")
FROM
	"Dns"."Record"
INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Dns"."Record"."DomainID"
	)
WHERE
	"Dns"."Record"."OrderName" != ''
		AND "Dns"."Record"."OrderName" IS NOT NULL
		AND "Dns"."Record"."DomainID" = $1
		AND "Dns"."Record"."Active" = TRUE
ORDER BY
	1 USING ~>~ LIMIT 1
;
