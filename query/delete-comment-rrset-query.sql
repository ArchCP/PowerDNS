DELETE FROM
	"Dns"."Comment"
WHERE
	"Dns"."Comment"."DomainID" = $1
		AND lower("Dns"."Comment"."Name") = lower($2)
		AND lower("Dns"."Comment"."Type") = lower($3)
;
