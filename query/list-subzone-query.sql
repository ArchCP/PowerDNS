SELECT
	"Dns"."Record"."Target",
	"Dns"."Record"."TTL",
	"Dns"."Record"."Priority",
	"Dns"."Record"."Type",
	"Dns"."Record"."DomainID",
	NOT "Dns"."Record"."Active",
	lower("Dns"."Record"."Name"),
	"Dns"."Record"."Authoritive"
FROM
	"Dns"."Record"
WHERE
	(lower("Dns"."Record"."Name") = lower($1) OR lower("Dns"."Record"."Name") LIKE lower($2))
	AND "Dns"."Domain"."ID" = $3
	AND "Dns"."Record"."Active" = TRUE
;
