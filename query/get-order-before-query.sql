SELECT
	"Dns"."Record"."OrderName",
	lower("Dns"."Record"."Name")
FROM
	"Dns"."Record"
WHERE
	"Dns"."Record"."OrderName" ~<=~ $1
		AND "Dns"."Record"."DomainID" = $2
		AND "Dns"."Record"."OrderName" IS NOT NULL
		AND "Dns"."Record"."Active" = TRUE
ORDER BY
	1 USING ~>~ LIMIT 1
;
