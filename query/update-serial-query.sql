UPDATE
	"Dns"."Domain"
SET
	"Serial" = $1
WHERE
	"Dns"."Domain"."ID" = $2
;
