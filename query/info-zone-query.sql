SELECT
	"Dns"."Domain"."ID",
	lower("Dns"."Domain"."Name"),
	"Dns"."Domain"."Master",
	"Dns"."Domain"."LastCheck",
	"Dns"."Domain"."Serial",
	"Dns"."Domain"."Type"
FROM
	"Dns"."Domain"
WHERE
	lower("Dns"."Domain"."Name") = lower($1)
		AND "Dns"."Domain"."Active" = TRUE
;
