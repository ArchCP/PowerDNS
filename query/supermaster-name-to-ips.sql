SELECT
	"Dns"."SuperMaster"."IP",
	"Dns"."SuperMaster"."AccountID"
FROM
	"Dns"."SuperMaster"
WHERE
	lower("Dns"."SuperMaster"."NameServer") = lower($1)
		AND "Dns"."SuperMaster"."AccountID" = $2
;
