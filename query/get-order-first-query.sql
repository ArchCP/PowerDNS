SELECT
	"Dns"."Record"."OrderName"
FROM
	"Dns"."Record"
WHERE
	"Dns"."Record"."DomainID" = $2
		AND "Dns"."Record"."OrderName" IS NOT NULL
		AND "Dns"."Record"."Active" = TRUE
ORDER BY
	1 USING ~>~ LIMIT 1
;
