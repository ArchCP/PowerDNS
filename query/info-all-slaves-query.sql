SELECT
	"Dns"."Domain"."ID",
	lower("Dns"."Domain"."Name"),
	"Dns"."Domain"."Master",
	"Dns"."Domain"."LastCheck",
	"Dns"."Domain"."Type"
FROM
	"Dns"."Domain"
WHERE
	lower("Dns"."Domain"."Type") = 'slave'
		AND "Dns"."Domain"."Active" = true
;
