SELECT
	"Dns"."Record"."Target",
	"Dns"."Record"."TTL",
	"Dns"."Record"."Priority",
	"Dns"."Record"."Type",
	"Dns"."Record"."DomainID",
	NOT "Dns"."Record"."Active",
	lower("Dns"."Record"."Name"),
	"Dns"."Record"."Authoritive"
FROM
	"Dns"."Record"
WHERE
	lower("Dns"."Record"."Type") = lower($1)
		AND lower("Dns"."Record"."Name") = lower($2)
		AND "Dns"."Record"."Active" = TRUE
;
