SELECT
	"Dns"."Record"."Target",
	"Dns"."Record"."TTL",
	"Dns"."Record"."Priority",
	"Dns"."Record"."Type",
	"Dns"."Record"."DomainID",
	NOT "Dns"."Record"."Active",
	lower("Dns"."Record"."Name"),
	"Dns"."Record"."Authoritive"
FROM
	"Dns"."Record"
WHERE
	("Dns"."Record"."Active" = TRUE OR $1::BOOLEAN)
		AND "Dns"."Record"."DomainID" = $2
		AND "Dns"."Record"."Active" = TRUE
ORDER BY
	"Dns"."Record"."Name", "Dns"."Record"."Type"
;
