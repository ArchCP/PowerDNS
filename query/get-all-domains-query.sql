SELECT
	"Dns"."Domain"."ID",
	"Dns"."Domain"."Name",
	"Dns"."Record"."Target",
	"Dns"."Domain"."Type",
	"Dns"."Domain"."Master",
	"Dns"."Domain"."Serial",
	"Dns"."Domain"."LastCheck",
	"Dns"."Domain"."AccountID"
FROM
	"Dns"."Domain"
LEFT JOIN "Dns"."Record" ON (
	"Dns"."Record"."DomainID" = "Dns"."Domain"."ID"
		AND lower("Dns"."Record"."Type") = 'soa'
		AND lower("Dns"."Record"."Name") = lower("Dns"."Domain"."Name")
)
WHERE
	("Dns"."Record"."Active" = TRUE OR $1::BOOLEAN)
;
