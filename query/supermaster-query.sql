SELECT
	"Dns"."SuperMaster"."AccountID"
FROM
	"Dns"."SuperMaster"
WHERE
	"Dns"."SuperMaster"."IP" = $1
		AND lower("Dns"."SuperMaster"."NameServer") = lower($2)
;
