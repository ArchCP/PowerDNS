UPDATE
	"Dns"."Domain"
SET
	"LastCheck" = $1
WHERE
	"Dns"."Domain"."ID" = $2
;
