SELECT
	"Dns"."MetaData"."Content"
FROM
	"Dns"."MetaData"
INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Dns"."MetaData"."DomainID"
	)
WHERE
	lower("Dns"."Domain"."Name") = lower($1)
		AND "Dns"."MetaData"."Kind" = $2
		AND "Dns"."Domain"."Active" = TRUE
;
