SELECT
	"Dns"."Comment"."DomainID",
	"Dns"."Comment"."Name",
	"Dns"."Comment"."Type",
	"Dns"."Comment"."ModifiedAt",
	"Dns"."Comment"."AccountID"
FROM
	"Dns"."Comment"
WHERE
	"Dns"."Comment"."DomainID" = $1
;
