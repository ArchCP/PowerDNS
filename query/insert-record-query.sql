INSERT INTO "Dns"."Record" ("Target", "TTL", "Priority", "Type", "DomainID", "Active", "Name", "OrderName", "ChangeDate") VALUES ($1, $2, $3, $4, $5, NOT $6::BOOLEAN, $7, $8, $9);
