SELECT
	"Dns"."Domain"."Master"
FROM
	"Dns"."Domain"
WHERE
	lower("Dns"."Domain"."Name") = lower($1)
		AND lower("Dns"."Domain"."Type") = 'slave'
		AND "Dns"."Domain"."Active" = TRUE
;
