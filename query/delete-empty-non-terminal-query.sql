delete from records where domain_id=$1 and name=$2 and type is null

DELETE FROM
	"Dns"."Record"
WHERE
	"DomainID" = $1
		AND lower("Record"."Name") = lower($2)
		AND "Record"."Type" IS NULL
;
