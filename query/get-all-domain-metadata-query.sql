SELECT
	"Dns"."MetaData"."Kind",
	"Dns"."MetaData"."Content"
FROM
	"Dns"."MetaData"
INNER JOIN
	"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Dns"."MetaData"."DomainID"
	)
WHERE
	LOWER("Dns"."Domain"."Name") = lower($1)
		AND "Dns"."Domain"."Active" = TRUE
;
