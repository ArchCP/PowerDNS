<?php
// Set the file path
$strMe = dirname(__FILE__);
// Define the destination configuration file
$strConfigFile = sprintf('%s/etc/powerdns/conf.d/gpgsql.conf', $strMe);
// Remove any existing configuration file
@unlink($strConfigFile);
// Create the file
copy(sprintf('%s/gpgsql.conf', $strMe), $strConfigFile);
// Iterate over the queries
foreach (glob(sprintf('%s/query/*.sql', $strMe)) as $strQueryFile) {
	// Set the query name
	$strQuery = sprintf('gpgsql-%s', str_ireplace([sprintf('%s/query/', $strMe), '.sql'], '', $strQueryFile));
	// Load the statement
	$strStatement = preg_replace('/(\n|\t+|\s+)/', ' ', file_get_contents($strQueryFile));
	// Remove multiple spaces
	$strStatement = trim(preg_replace('/\s+/', ' ', $strStatement));
	// Write the statement to the configuration file
	file_put_contents($strConfigFile, sprintf('%s = %s%s%s', $strQuery, $strStatement, PHP_EOL, PHP_EOL), FILE_APPEND);
}