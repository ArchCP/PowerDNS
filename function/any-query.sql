-- Define our new function
CREATE OR REPLACE FUNCTION "any_query"(qtype VARCHAR(255), qname VARCHAR(255)) RETURNS SETOF "pdns_any_query" LANGUAGE plpgsql AS $$
-- Begin the statement
BEGIN
	-- Return the select statement
	RETURN QUERY SELECT
		"Dns"."Record"."Target" AS "content",
		"Dns"."Record"."TTL" AS "ttl",
		"Dns"."Record"."Priority" AS "prio",
		"Dns"."Record"."Type" AS "type",
		"Dns"."Record"."DomainID" AS "domain_id",
		NOT "Dns"."Record"."Active" AS "disabled",
		(LOWER("Dns"."Record"."Name") || '.' || LOWER("Dns"."Domain"."Name")) AS "name",
		"Dns"."Record"."Authoritive" AS "auth"
	FROM
		"Dns"."Record"
	INNER JOIN
		"Dns"."Domain" ON (
		"Dns"."Domain"."ID" = "Dns"."Record"."DomainID"
		)
	WHERE
		(
			(LOWER("Dns"."Record"."Name") || '.' || LOWER("Dns"."Domain"."Name")) = LOWER(qname)
				OR
			LOWER("Dns"."Domain"."Name") = LOWER(qname)
		)
		AND "Dns"."Record"."Active" = true
	;
-- End The statement
END
-- Finish the function
$$;
