-- Define our function
CREATE OR REPLACE FUNCTION "list_subzone_query"(qname VARCHAR(255), qnamesearch VARCHAR(255), qid BIGINT) RETURNS SETOF "pdns_any_query" LANGUAGE plpgsql AS $$
-- Begin the statement
BEGIN
	-- Return the SELECT statement
	RETURN QUERY
-- End the statement
END
-- End Function Definition
$$;
