-- Define our type to match the default PowerDNS record
CREATE TYPE "pdns_any_query" AS (
	"content" TEXT,
	"ttl" INTEGER,
	"prio" INTEGER,
	"type" VARCHAR(10),
	"domain_id" BIGINT,
	"disabled" BOOLEAN,
	"name" TEXT,
	"auth" BOOLEAN
);
